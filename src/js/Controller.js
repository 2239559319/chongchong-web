import MidiPlayer from "./MidiPlayer";
import NativeMidiPlayer from "./NativeMidiPlayer";
import Signal from "./Signal";
const JSZip = require("jszip");
// import { saveAs } from 'file-saver';
const Base64 = require('js-base64').Base64

export default class Controller {
  constructor(vue, viewCallback, nativeMode) {
    this.vue = vue
    this.viewCallback = viewCallback
    this.signal = new Signal(vue)
    this.nativeMode = nativeMode
    let SELF = this
    window.addEventListener("resize", () => {
      console.log("onResize");
      if (SELF.svgScore) {
        SELF.svgScore.onrotate();
        SELF.vue.$nextTick(() => {
          SELF.refreshPosition()
        });
      }
    }, false);
    window.onorientationchange = () => {
      if (SELF.svgScore) {
        SELF.svgScore.onrotate();
        SELF.vue.$nextTick(() => {
          SELF.refreshPosition()
        });
      }
    };
  }
  isFromApp() {
    return this.signal.isApp()
  }
  setSvgScore(svgScore) {
    this.svgScore = svgScore
  }
  setWaterfall(waterfall) {
    this.waterfall = waterfall
    if (this.player) {
      this.player.setWaterfall(this.waterfall)
      this.player.setWaterfallMode(this.waterfallMode)
    }
    this.setWaterfallMode(this.waterfallMode)
  }
  setWaterfallMode(waterfallMode) {
    this.waterfallMode = waterfallMode
    this.updateState({ waterfallMode: this.waterfallMode }, "waterfallMode");
    if (this.viewCallback) {
      this.viewCallback.onWaterfallMode(this.waterfallMode)
    }
    if (this.player) {
      this.player.setWaterfallVisible(this.waterfallMode)
    }
  }
  getWaterfallMode() {
    return this.waterfallMode
  }
  readTextFile(file, readingMode, callback) {
    this.readingMode = readingMode
    let SELF = this
    var rawFile = new XMLHttpRequest();
    rawFile.responseType = "arraybuffer";
    rawFile.open("GET", file, true);
    rawFile.onload = () => {
      console.log("reading ccmz ok:", new Date().getTime());
      SELF.readCCMZ(rawFile.response, callback);
    };
    // 请求出错
    rawFile.onerror = () => {
      console.log("request error:", rawFile.status, ",", rawFile.statusText);
    };
    rawFile.send();
  }
  readCCMZ(buffer, callback) {
    let version = (new Uint8Array(buffer.slice(0, 1)))[0];
    console.log("ccmz version:", version);
    let data = new Uint8Array(buffer.slice(1))
    if (version == 1) {
      JSZip.loadAsync(data).then((zip) => {
        zip
          .file("data.ccxml")
          .async("string")
          .then((json) => {
            let score = JSON.parse(json);
            callback(score)
            zip
              .file("data.ccmid")
              .async("string")
              .then((json) => {
                this.initPlayer(json)
              });
          });
      });
    } else if (version == 2) {
      data = data.map((value) => {
        return value % 2 == 0 ? value + 1 : value - 1
      })
      JSZip.loadAsync(data).then((zip) => {
        zip
          .file("score.json")
          .async("string")
          .then((json) => {
            let score = JSON.parse(json);
            callback(score)
            zip
              .file("midi.json")
              .async("string")
              .then((json) => {
                this.initPlayer(json)
              });
          });
      });
    }
  }

  setDataCallback(callback) {
    this.callback = callback
  }

  setupXml(data) {
    let json = Base64.decode(data)
    let score = JSON.parse(json);
    if (this.callback)
      this.callback(score)
    this.player = new NativeMidiPlayer(this.signal)
    if (this.svgScore) {
      this.updateState(
        { pageCount: this.svgScore.getPages().length },
        "pageCount"
      );
    }
    this.player.refreshState("inited", 1);
  }
  initPlayer(json) {
    let SELF = this
    this.player = new MidiPlayer(
      (data, name) => {
        SELF.onStateCallback(data, name)
      },
      (measure, notes) => {
        SELF.onMeasureCallback(measure, notes)
      },
      (state, note, track) => {
        SELF.onNoteCallback(state, note, track)
      },
      (measureIndex, noteIndex, percent) => {
        SELF.onCursorCallback(measureIndex, noteIndex, percent)
      }
    );
    this.player.init(JSON.parse(json))
    if (this.waterfall) {
      this.player.setWaterfall(this.waterfall)
      this.player.setWaterfallVisible(this.waterfallMode)
    }

    if (this.svgScore) {
      this.updateState(
        { pageCount: this.svgScore.getPages().length },
        "pageCount"
      );
    }

    if (this.readingMode) {
      this.player.refreshState("inited", 2);
    } else {
      this.player.refreshState("inited", 1);
      // this.svgScore.showCursor(0, 0, 0);
      console.log("loadsoundfont");
      this.player.loadSoundfont();
    }
  }

  isPlayerReady() {
    return !this.readingMode && this.player && this.player.ready()
  }

  onMeasureCallback(measure, notes) {
    this.signal.send("onMeasureCallback", {
      measure: measure,
      notes: notes,
    });
  }
  onNoteCallback(state, note, track) {
    this.signal.send("onNoteCallback", {
      state: state,
      note: note,
      track: track,
    });
  }
  updateState(data, name) {
    this.signal.send("onUpdateState", { data: data, name: name });
  }
  onStateCallback(data, name) {
    if (name == "inited") {
      console.log("inited");
      // this.svgScore.showCursor(0, 0, 0);
      if (data.inited == 1) {
        // this.loadSoundfont()
      }
    }
    this.updateState(data, name);
  }

  onCursorCallback(measureIndex, noteIndex, percent) {
    let pos = this.showCursor(
      measureIndex,
      noteIndex,
      percent
    );
    if (pos && (!this.lastPos || this.lastPos.y != pos.y) && !this.locked) {
      this.scrollToCursor(pos);
      this.lastPos = pos;
    }
  }
  getOffsetTop(el) {
    return el.offsetParent ? el.offsetTop + this.getOffsetTop(el.offsetParent) : el.offsetTop
  }
  scrollToCursor(measurePos, animate = true) {
    let offsetY = this.getOffsetTop(this.svgScore.$el.parentElement);
    let firstMeasurePos = this.svgScore.musScore.getMueasurePos(0);
    let target = offsetY +
      (measurePos.y - firstMeasurePos.y) * this.svgScore.musScore.scale;
    if (Math.abs(window.orientation) % 180 == 90) {
      console.log("orientation=", window.orientation);
      console.log("scale=", this.svgScore.musScore.scale);
      console.log(
        "document.documentElement.clientHeight=",
        document.documentElement.clientHeight
      );
      target = offsetY +
        measurePos.y * this.svgScore.musScore.scale -
        (document.documentElement.clientHeight -
          measurePos.height * this.svgScore.musScore.scale) /
        2;
    }
    target = Math.max(0, target);
    if (animate) this.scrollAnimation(window.scrollY, target);
    else window.scrollTo(0, target);
  }
  scrollAnimation(currentY, targetY) {
    // 获取当前位置方法
    // const currentY = document.documentElement.scrollTop || document.body.scrollTop

    // 计算需要移动的距离
    let needScrollTop = targetY - currentY;
    let _currentY = currentY;
    setTimeout(() => {
      // 一次调用滑动帧数，每次调用会不一样
      const dist = Math.ceil(needScrollTop / 10);
      _currentY += dist;
      window.scrollTo(0, currentY);
      // 如果移动幅度小于十个像素，直接移动，否则递归调用，实现动画效果
      if (needScrollTop > 10 || needScrollTop < -10) {
        this.scrollAnimation(_currentY, targetY);
      } else {
        window.scrollTo(0, targetY);
      }
    }, 1);
  }

  refreshPosition() {
    if (this.lastPos) {
      this.scrollToCursor(this.lastPos, false);
    }
  }
  playOrPause() {
    if (this.player.isPlaying()) {
      this.player.pausePlay();
    } else {
      // this.player.mute(2)
      // this.player.mute(3)
      this.player.startPlay();
    }
  }
  startPlay() {
    if (this.player && !this.player.isPlaying()) {
      this.player.startPlay();
    }
  }
  pausePlay() {
    if (this.player && this.player.isPlaying()) {
      this.player.pausePlay();
    }
  }
  isPlaying() {
    return this.player ? this.player.isPlaying() : false;
  }

  onTouchStart(event) {
    this.svgScore.onTouchStart(event);
  }
  onTouchEnd(event) {
    this.svgScore.onTouchEnd(event);
  }
  onMouseDown(event) {
    this.svgScore.onMouseDown(event);
  }
  onMouseUp(event) {
    this.svgScore.onMouseUp(event);
  }
  clickmeas(click) {
    console.log("touched measure", click);
    if (click == undefined || this.locked) return;
    if (!this.loopABStep) {
      this.player.placeTo(click.mm, click.nn);
      this.showCursor(click.mm, click.nn, 0);
      return;
    }
    if (this.loopABStep == 1) {
      this.loopABStart = click.mm;
      let rect = this.svgScore.getMeasureRect(click.mm)
      this.viewCallback.showLoop(rect)
      this.svgScore.highlightMeasure([click.mm], "white");
      this.loopABStep = 2
      this.updateState({ loopABStep: this.loopABStep }, "loopABStep");
    } else if (this.loopABStep == 2) {
      this.loopABEnd = click.mm;
      let min = Math.min(this.loopABStart, click.mm);
      let max = Math.max(this.loopABStart, click.mm);
      let arr = [];
      for (let i = min; i <= max; i++) {
        arr.push(i);
      }
      let rectA = this.svgScore.getMeasureRect(arr[0])
      let rectB = this.svgScore.getMeasureRect(arr[arr.length - 1])
      this.viewCallback.showLoop(rectA, rectB)
      this.svgScore.highlightMeasure(arr, "white");
      this.player.loopABMeasure(arr[0], arr[arr.length - 1]);
      this.loopABStep = 3
      this.updateState({ loopABStep: this.loopABStep }, "loopABStep");
    }
  }
  loopABOn() {
    this.loopABStep = 1;
    this.updateState({ loopABStep: this.loopABStep }, "loopABStep");
    this.viewCallback.showLoop()
  }
  loopABOff() {
    this.loopABStep = 0;
    this.updateState({ loopABStep: this.loopABStep }, "loopABStep");
    this.svgScore.highlightMeasure([]);
    this.player.loopABCancel();
    this.viewCallback.showLoop()
  }
  setLoopAll(loopAll) {
    this.player.setLoopAll(loopAll);
  }
  isLoopAll() {
    return this.player.isLoopAll();
  }
  setSpeed(speed) {
    this.player.setSpeed(speed);
  }
  getSpeed() {
    return this.player.getSpeed();
  }
  setTransposition(transpotion) {
    this.player.setTransposition(transpotion);
  }
  getTransposition() {
    return this.player.getTransposition();
  }
  setVolume(volume) {
    this.player.setVolume(volume);
  }
  getVolume() {
    return this.player.getVolume();
  }
  getDuration() {
    return this.player.getDuration();
  }
  getCurrentTime() {
    return Math.max(0, this.player.getCurrentTime());
  }
  seekTo(progress) {
    this.player.seekTo(progress);
  }
  setJianpuMode(jianpu) {
    console.log("jianpu:", jianpu);
    if (jianpu === 'true' || jianpu === true) jianpu = 1;
    else if (jianpu === 'false' || jianpu === false) jianpu = 0;
    this.svgScore.showJianpu(jianpu);
    this.updateState({ isJianpuMode: jianpu != 0 }, "isJianpuMode");
    this.updateState({ jianpuMode: jianpu }, "jianpuMode");
  }
  isJianpuMode() {
    return this.svgScore.jianpu != 0;
  }
  getJianpuMode() {
    return this.svgScore.jianpu;
  }
  resetPlay() {
    this.player.resetPlay();
    if (!this.locked) {
      this.showCursor(0, 0, 0);
      window.scrollTo(0, 0);
    }
  }
  showCursor(mm, nn, percent) {
    if (!this.svgScore) return
    let pos = this.svgScore.showCursor(mm, nn, percent);
    if (this.viewCallback) {
      this.viewCallback.showCursor(pos)
    }
    return pos
  }
  setCursorVisibility(visible) {
    if (this.viewCallback) {
      if (visible == true || visible == 'true')
        this.viewCallback.showCursor()
      else
        this.viewCallback.hideCursor()
    }
  }
  getTrackCount() {
    return this.player.getTrackCount();
  }
  switchTrack() {
    this.player.switchTrack();
    this.renewTrackColor();
  }
  setEnabledTrack(track) {
    this.player.setEnabledTrack(track);
    this.renewTrackColor();
  }
  renewTrackColor() {
    let track = this.player.getEnabledTrack();
    let trackCount = this.player.getTrackCount();
    let disabledTrack = [];
    if (track != -1) {
      for (let i = 0; i < trackCount; i++) {
        if (i != track) disabledTrack.push(i);
      }
    }
    this.svgScore.setTrackStyle(disabledTrack, { color: "gray" });
  }
  noteOn(note) {
    this.player.noteOn(note);
  }
  moveToNextChord() {
    let measure = this.player.getCurrentMeasure();
    let pos = this.svgScore.musScore.getMueasurePos(measure);
    console.log("measure ", measure);
    this.scrollToCursor(pos);
    this.player.moveToNextChord();
  }
  setMetronomeOn(metronomeOn) {
    this.player.setMetronomeOn(metronomeOn);
  }
  isMetronomeOn() {
    return this.player.isMetronomeOn();
  }
  isLocked() {
    return this.locked
  }
  setLocked(locked) {
    this.locked = locked;
    if (locked) {
      document.body.addEventListener('touchmove', this.scrollListener, { passive: false });
      document.body.addEventListener('mousemove', this.scrollListener, { passive: false });
      document.body.addEventListener('scroll', this.scrollListener, { passive: false });
      document.body.addEventListener('wheel', this.scrollListener, { passive: false });
    } else {
      document.body.removeEventListener('touchmove', this.scrollListener, { passive: false });
      document.body.removeEventListener('mousemove', this.scrollListener, { passive: false });
      document.body.removeEventListener('scroll', this.scrollListener, { passive: false });
      document.body.removeEventListener('wheel', this.scrollListener, { passive: false });
    }
    this.updateState({ locked: locked }, "locked");
  }
  scrollListener(event) {
    event.preventDefault();
  }
}