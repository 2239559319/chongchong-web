export default class Signal {
    constructor(component) {
        this.component = component
        if (navigator.userAgent.indexOf('ccpiano-android') != -1) {
            this.platform = 'android'
        } else if (navigator.userAgent.indexOf('ccpiano-ios') != -1) {
            this.platform = 'ios'
        }
        if (this.platform == 'ios') {
            let originLog = console.log;
            console.log = function () {
                let str = Array.prototype.join.call(arguments, "");
                originLog(str);
                window.webkit.messageHandlers.logger.postMessage(str);
            };
        }
    }

    send(method, data) {
        if (this.platform == 'ios') {
            window.webkit.messageHandlers[method].postMessage(JSON.stringify(data));
        } else if (this.platform == 'android') {
            window.android[method](JSON.stringify(data));
        } else {
            this.component.$emit(method, data);
        }
    }
    isApp() {
        return this.platform == 'ios' || this.platform == 'android'
    }
}