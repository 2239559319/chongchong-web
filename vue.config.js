/**
 * @type {import('@vue/cli-service').ProjectOptions}
 */
const config = {
  devServer: {
    proxy: {
      '/sounds/': {
        target: 'https://www.gangqinpu.com/sheetplayer',
        changeOrigin: true
      },
      '/soundfont/': {
        target: 'https://www.gangqinpu.com/sheetplayer',
        changeOrigin: true
      }
    }
  }
}

module.exports = config
